from models.car import Database
import json

db = Database()


# function yang digunakan untuk merubah ObjectId ke String
def ObjToStr(obj):
    try:
        return str(obj["_id"]["$oid"])
    except Exception as e:
        print(f"kesalahan function ObjToStr: {e}")


# function yang digunakan untuk insertData
def tambahDataCar(**params):
    try:
        db.insertData(**params)
        data = {
            "message" : "data berhasil ditambahkan"
        }
        return data
    except Exception as e:
        print(f"kesalahan function insertData: {e}")


# function yang digunakan untuk melihat semua data car
def viewCars():
    try:
        dbhasil = db.showCars()
        hasil = json.loads(dbhasil)
        data_list = []
        for car in hasil:
            car["id_car"] = ObjToStr(car)
            data_list.append(car)
        return data_list
    except Exception as e:
        print(f"kesalahan function viewCars: {e}")


# function yang digunakan untuk melihat data car by params
def viewCarsByParams(**params):
    try:
        dbhasil = db.showCarByParams(**params)
        hasil = json.loads(dbhasil)
        data_list = []
        if type(hasil) == list:
            for car in hasil:
                car["id_car"] = ObjToStr(car)
                data_list.append(car)
            return data_list
        else:
            hasil["id_car"] = ObjToStr(hasil)
            return hasil
    except Exception as e:
        print(f"kesalahan function viewCarsByParams{e}")


# function yang digunakan untuk update data car
def ubahDataCarById(**params):
    try:
        db.updateCarById(**params)
        data = {
            "message" : "data berhasil diubah"
        }
        return data
    except Exception as e:
        print(f"kesalahan function ubahDataById: {e}")


# function yang digunakan untuk menghapus data car by id_car
def hapusCarById(**params):
    try:
        db.deleteCarById(**params)
        data = {
            "message" : "data berhasil dihapus"
        }
        return data
    except Exception as e:
        print(f"kesalahan function deleteCarById: {e}")