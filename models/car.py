from mongoengine import connect, Document, StringField, DynamicDocument, IntField

class Car(Document):
    nama_mobil = StringField(required=True, max_length=255)
    warna_mobil = StringField(required=True, max_length=255)
    harga_mobil = IntField(required=True)


class Database:
    # method yang digunakan untuk koneksi ke database MongoDB
    def __init__(self):
        try:
            self.connection = connect(
                db="jualmobil",
                host="localhost",
                port=27017
            )
        except Exception as e:
            print(f"gagal koneksi ke database MongoDB{e}")

    # method yang digunakan untuk insert data car ke database
    def insertData(self, **params):
        try:
            Car(**params).save()
        except Exception as e:
            print(f"kesalahan function inserData: {e}")


    # method yang digunakan untuk melihat semua data dalam database car
    def showCars(self):
        try:
            return Car.objects().all().to_json()
        except Exception as e:
            print(f"kesalahan function showCars: {e}")


    # method yang digunakan untuk melihat data by params
    def showCarByParams(self, **params):
        try:
            if "id_car" in params.keys():
                return Car.objects().get(id=params["id_car"]).to_json()
            else:
                if "nama_mobil" in params.keys():
                    return Car.objects(nama_mobil__icontains = params["nama_mobil"]).to_json()
                if "warna_mobil" in params.keys():
                    return Car.objects(warna_mobil__icontains = params["warna_mobil"]).to_json()
        except Exception as e:
            print(f"kesalahan function showCarByParams: {e}")


    # mehtod yang digunakan untuk update data car
    def updateCarById(self, **params):
        try:
            dataCar = Car.objects().get(id = params["id_car"])
            if "nama_mobil" in params.keys():
                dataCar.nama_mobil = params["nama_mobil"]
            if "warna_mobil" in params.keys():
                dataCar.warna_mobil = params["warna_mobil"]
            if "harga_mobil" in params.keys():
                dataCar.harga_sewa = params["harga_mobil"]
            dataCar.save()  # memberitahu database
        except Exception as e:
            print(f"kesalahan function updateCarById: {e}")


    # method yang digunakan untuk menghapus data car dari database
    def deleteCarById(self, **params):
        try:
            dataCar = Car.objects().get(id = params["id_car"])
            dataCar.delete()
        except Exception as e:
            print(f"kesalahan{e}")