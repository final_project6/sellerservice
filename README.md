# sellerService
berisi service untuk menangani seller, data yang berisi adalah data mobil. dalam service ini kita dapat melihat data mobil, menambahkan data mobil, negubah data mobil apabila terjadi kesalahan input, dan menghapus data mobil.

## Cara menjalankan
untuk menjalankan, ketikkan `uvicorn app:app --reload` pada comand prompt, akan berjalan pada port 8000

## Routing API
### Insert Data Car
`http://localhost:8000/car/insert` method POST

data = { <br>
    "nama_mobil" : "Mitsubishi Pajero Sport Dakar 2019", <br>
    "warna_mobil" : "Hitam", <br>
    "harga_mobil" : 550000000 <br>
}

### Show All Cars
`http://localhost:8000/cars` method GET

### Show Car By Params
`http://localhost:8000/car` method GET

data = { <br>
    "id_car" : "613c96dfd061a55e37571697", <br>
    "nama_mobil" : "....." (opsional), <br>
    "warna_mobil" : "....." (opsional) <br>
}

### Update Data Car
`http://localhost:8000/car/update` method POST

data = {<br>
    "id_car" : "613c96dfd061a55e37571697", <br>
    #.. key - value yang akan diupdate ..# <br>
}

### Delete Data Car
`http://localhost:8000/car/delete` method POST

data = { <br>
    "id_car" : "613c975fd061a55e3757169c" <br>
}