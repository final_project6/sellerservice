from fastapi import FastAPI
from controllers import car

app = FastAPI()

# route API untuk insert data car ke database
@app.post("/car/insert")
async def carInsertService(params: dict):
    try:
        return car.tambahDataCar(**params)
    except Exception as e:
        message = {
            "message" : "kesalahan pada input data",
            "error" : e
        }
        return message


# route API untuk melihat semua data car
@app.get("/cars")
async def carsService():
    try:
        return car.viewCars()
    except Exception as e:
        message = {
            "message" : "kesalahan function carsService",
            "error" : e
        }
        return message

# route API yang digunakan untuk melihat data car by parameter
@app.get("/car")
async def carsByParamsService(params: dict):
    try:
        return car.viewCarsByParams(**params)
    except Exception as e:
        message = {
            "message" : "kesalahan function carsByParamsService",
            "error" : e
        }
        return message


# route API yang digunakan untuk mengubah data car
@app.post("/car/update")
async def carUpdateService(params: dict):
    try:
        return car.ubahDataCarById(**params)
    except Exception as e:
        message = {
            "message" : "kesalahan function carUpdateService",
            "error" : e
        }
        return message


# route API yang digunakan untuk menghapus data car
@app.post("/car/delete")
async def carDeleteService(params: dict):
    try:
        return car.hapusCarById(**params)
    except Exception as e:
        message = {
            "message" : "kesalahan function carDeleteService",
            "error" : e
        }
        return message